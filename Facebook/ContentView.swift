//
//  ContentView.swift
//  Facebook
//
//  Created by Тимур Чеберда on 02.11.2019.
//  Copyright © 2019 Tmur Cheberda. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
